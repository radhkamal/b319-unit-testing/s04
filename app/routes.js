const { names, users } = require('../src/util.js');

module.exports = (app) => {

	app.get('/', (req, res) => {
		return res.send({'data': {}});
	});

	app.get('/people', (req, res) => {
		return res.send({
			people: names
		});
	});

	app.get('/users', (req, res) => {
		return res.send({
			people: names
		});
	});


	app.post('/person', (req, res) => {

		if(!req.body.hasOwnProperty('name')) {
			return res.status(400).send({
				'error': "Bad Request - missing required parameter NAME"
			});
		}

		if(typeof req.body.name !== 'string') {
			return res.status(400).send({
				'error': "Bad Request - name has to be a string"
			});
		}

		if(!req.body.hasOwnProperty('age')) {
			return res.status(400).send({
				'error': "Bad Request - missing required parameter AGE"
			});
		}

		if(typeof req.body.age !== 'number') {
			return res.status(400).send({
				'error': "Bad Request - age has to be a number"
			});
		}
	})

	app.post('/users', (req, res) => {

		if(req.body.hasOwnProperty('name') && req.body.hasOwnProperty('username') && req.body.hasOwnProperty('age')) {
			return res.status(200).send({
				user: req.body
			});
		}

		if(!req.body.hasOwnProperty('username')) {
			return res.status(400).send({
				'error': "Bad Request - missing required parameter USERNAME"
			});
		}

		if(!req.body.hasOwnProperty('age')) {
			return res.status(400).send({
				'error': "Bad Request - missing required parameter AGE"
			});
		}
	});

	app.post('/login', (req, res) => {

		if(req.body.hasOwnProperty('username') && req.body.hasOwnProperty('password')) {
			return res.status(200).send({
				user: req.body
			});
		}

		if(!req.body.hasOwnProperty('username')) {
			return res.status(400).send({
				'error': "Bad Request - missing required parameter USERNAME"
			});
		}

		if(!req.body.hasOwnProperty('password')) {
			return res.status(400).send({
				'error': "Bad Request - missing required parameter PASSWORD"
			});
		}
	})
}

