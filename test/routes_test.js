const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('api_test_suite', () => {

	it('test_api_get_people_is_running', () => {
		chai.request('http://localhost:5294').get('/people').end((err, res) => {
			expect(res).to.not.equal(undefined);
		});
	});

	it('test_api_get_people_returns_200', (done) => {
		chai.request('http://localhost:5294').get('/people').end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		});
	});

	it('test_api_post_person_returns_400_if_no_person_name', (done) => {
		chai.request('http://localhost:5294')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	});

	it('test_api_post_person_returns_400_if_age_is_string', (done) => {
		chai.request('http://localhost:5294')
		.post('/person')
		.type('json')
		.send({
			name: "James",
			age: "Twenty-Eight"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})


})

describe('api_test_suite_for_users_route', () => {

	it('test_api_post_users_returns_200', (done) => {
		chai.request('http://localhost:5294')
		.post('/users')
		.type('json')
		.send({
			name: "Victoria Pedretti",
			username: "victoria.pedretti",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		});
	});

	it('test_api_post_users_returns_400_if_username_is_missing', (done) => {
		chai.request('http://localhost:5294')
		.post('/users')
		.type('json')
		.send({
			name: "Victoria Pedretti",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	});

	it('test_api_post_users_returns_400_if_age_is_missing', (done) => {
		chai.request('http://localhost:5294')
		.post('/users')
		.type('json')
		.send({
			name: "Victoria Pedretti",
			username: "victoria.pedretti",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

})

describe('api_test_suite_for_login_route', () => {

	it('test_api_post_login_returns_400_if_no_username', (done) => {
		chai.request('http://localhost:5294')
		.post('/users')
		.type('json')
		.send({
			password: "victoria.pedretti"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	});

	it('test_api_post_login_returns_400_if_no_password', (done) => {
		chai.request('http://localhost:5294')
		.post('/users')
		.type('json')
		.send({
			username: "victoria.pedretti",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('test_api_post_login_returns_200_if_correct_credentials', (done) => {
		chai.request('http://localhost:5294')
		.post('/login')
		.type('json')
		.send({
			username: "victoria.pedretti",
			password: "victoria.pedretti"
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		});
	});

})